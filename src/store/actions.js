export const ADD_POLL_QUESTION = "ADD_POLL_QUESTION";
export const DELETE_POLL_QUESTION = "DELETE_POLL_QUESTION";
export const FETCH_POLL_QUESTIONS = "FETCH_POLL_QUESTIONS";
export const SET_POLL_QUESTIONS = "SET_POLL_QUESTIONS";
export const SET_LOADING = "SET_LOADING";
export const SET_SETTINGS = "SET_SETTINGS";
export const SUBSCRIBE_SETTINGS = "SUBSCRIBE_SETTINGS";
