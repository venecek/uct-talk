module.exports = {
  purge: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  darkMode: false,
  theme: {
    extend: {
      spacing: {
        "56.25": "56.25%",
      },
      fontFamily: {
        poppins: ["Poppins"],
      },
      animation: {
        progress: "progress linear forwards",
        "fade-out": "fade-out 250ms linear forwards",
        "fade-in": "fade-in 250ms linear forwards",
      },
      keyframes: {
        progress: {
          "0%": { width: "0" },
          "100%": { width: "100%" },
        },
        "fade-out": {
          "100%": { opacity: 0 },
        },
        "fade-in": {
          "0%": { opacity: 0 },
          "100%": { opacity: 1 },
        },
      },
      fontSize: {
        "2xs": "0.65rem",
      },
    },
  },
  variants: {
    extend: {
      borderStyle: ["hover"],
    },
  },
  plugins: [],
};
